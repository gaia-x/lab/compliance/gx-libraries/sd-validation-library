import fs from "fs";
import https from "https";
import fetch from 'node-fetch';
import factory from "rdf-ext";
import ParserN3 from "@rdfjs/parser-n3";
import ParserJsonld from "@rdfjs/parser-jsonld";
import SHACLValidator from "rdf-validate-shacl";
import { Readable } from "stream";

const shapes = [
    {
        type: "gax-resource:Node",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=nodeShape.json" }
        ]
    },
    {
        type: "gax-resource:SSD",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=ssdShape.json" }
        ]
    },
    {
        type: "gax-participant:Participant",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=participantShape.json" }
        ]
    },
    {
        type: "gax-resource:BareMetalNode",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=bare-metal-nodeShape.json" }
        ]
    },
    {
        type: "gax-resource:Resource",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=resourceShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOfferingSubCompanies",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offering-sub-companiesShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOffering",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offeringShape.json" }
        ]
    },
    {
        type: "gax-resource:Route",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=routeShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOfferingSecurity",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offering-securityShape.json" }
        ]
    },
    {
        type: "gax-resource:PhysicalMedium",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=physical-mediumShape.json" }
        ]
    },
    {
        type: "gax-resource:KubernetesNode",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=k8sNodeShape.json" }
        ]
    },
    {
        type: "gax-service:DataAccessStandard",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=data-access-standardShape.json" }
        ]
    },
    {
        type: "gax-participant:Provider",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=providerShape.json" }
        ]
    },
    {
        type: "gax-resource:Interconnection",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=interconnectionShape.json" }
        ]
    },
    {
        type: "gax-resource:HardDrive",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=hard-driveShape.json" }
        ]
    },
    {
        type: "gax-service:Compute",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=computeShape.json" }
        ]
    },
    {
        type: "gax-resource:GPU",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=gpuShape.json" }
        ]
    },
    {
        type: "gax-service:Storage",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=storageShape.json" }
        ]
    },
    {
        type: "gax-service:DataServiceOffering",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=data-service-offeringShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOfferingCertificates",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offering-certificatesShape.json" }
        ]
    },
    {
        type: "gax-resource:BareMetalServiceOffering",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=bare-metal-service-offeringShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOfferingInteroperability",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offering-interoperabilityShape.json" }
        ]
    },
    {
        type: "gax-resource:Connection",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=connectionShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOfferingDataProtection",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offering-data-protectionShape.json" }
        ]
    },
    {
        type: "gax-service:InfrastructureServiceOffering",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=infrastructureServiceOfferingShape.json" }
        ]
    },
    {
        type: "gax-service:DataResource",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=data-resourceShape.json" }
        ]
    },
    {
        type: "gax-unknown:unknown",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=k8saasShape.json" }
        ]
    },
    {
        type: "gax-service:Networking",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=networkingShape.json" }
        ]
    },
    {
        type: "gax-core:HardwareSpec",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=hardware-specShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOfferingOperativeProcesses",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offering-operative-processesShape.json" }
        ]
    },
    {
        type: "gax-service:ServiceOffering",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=service-offeringShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOfferingArchitecture",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offering-architectureShape.json" }
        ]
    },
    {
        type: "gax-core:Standard",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=standardShape.json" }
        ]
    },
    {
        type: "gax-service:DataModellingStandard",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=data-modelling-standardShape.json" }
        ]
    },
    {
        type: "gax-service:TrustedCloudServiceOfferingContracts",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-service-offering-contractsShape.json" }
        ]
    },
    {
        type: "gax-resource:CPU",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=cpuShape.json" }
        ]
    },
    {
        type: "gax-resource:NetworkingDevice",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=networking-deviceShape.json" }
        ]
    },
    {
        type: "gax-participant:TrustedCloudProvider",
        shapes: [
            { url: "https://gaia-x.fit.fraunhofer.de:8443/getJSON?name=trusted-cloud-providerShape.json" }
        ]
    },
    {
        type: "gax-participant:legal-person",
        validate: validation_gaxParticipantLegalPerson,
        shapes: [
            { url: "https://registry.lab.gaia-x.eu/shapes/v1/participant.ttl" }
        ]
    },
    {
        type: "ctx:sd-document",
        validate: validation_gaxParticipantLegalPerson,
        shapes: [
            { url: "https://kb.crossloom.com/repository/gx_schema_repo/filename/ctx-sd-document_shape.ttl" },
            { url: "https://kb.crossloom.com/repository/gx_schema_repo/filename/gax-participant-legal-person_shape.ttl" }
        ]
    }
];

const iso3166List = ["AF", "AFG", "004",
    "EG", "EGY", "818",
    "AL", "ALB", "008",
    "DZ", "DZA", "012",
    "VI", "VIR", "850",
    "AS", "ASM", "016",
    "AD", "AND", "020",
    "AO", "AGO", "024",
    "AI", "AIA", "660",
    "AG", "ATG", "028",
    "GQ", "GNQ", "226",
    "AR", "ARG", "032",
    "AM", "ARM", "051",
    "AW", "ABW", "533",
    "AZ", "AZE", "031",
    "ET", "ETH", "231",
    "AU", "AUS", "036",
    "BS", "BHS", "044",
    "BH", "BHR", "048",
    "BD", "BGD", "050",
    "BB", "BRB", "052",
    "BY", "BLR", "112",
    "BE", "BEL", "056",
    "BZ", "BLZ", "084",
    "BJ", "BEN", "204",
    "BM", "BMU", "060",
    "BT", "BTN", "064",
    "BO", "BOL", "068",
    "BQ", "BES", "535",
    "BA", "BIH", "070",
    "BW", "BWA", "072",
    "BR", "BRA", "076",
    "VG", "VGB", "092",
    "BN", "BRN", "096",
    "BG", "BGR", "100",
    "BF", "BFA", "854",
    "BI", "BDI", "108",
    "CV", "CPV", "132",
    "CL", "CHL", "152",
    "CN", "CHN", "156",
    "CK", "COK", "184",
    "CR", "CRI", "188",
    "CI", "CIV", "384",
    "CW", "CUW", "531",
    "DK", "DNK", "208",
    "DE", "DEU", "276",
    "DM", "DMA", "212",
    "DO", "DOM", "214",
    "DJ", "DJI", "262",
    "EC", "ECU", "218",
    "SV", "SLV", "222",
    "ER", "ERI", "232",
    "EE", "EST", "233",
    "SZ", "SWZ", "748",
    "FK", "FLK", "238",
    "FO", "FRO", "234",
    "FJ", "FJI", "242",
    "FI", "FIN", "246",
    "FR", "FRA", "250",
    "GF", "GUF", "254",
    "PF", "PYF", "258",
    "GA", "GAB", "266",
    "GM", "GMB", "270",
    "PS", "PSE", "275",
    "GE", "GEO", "268",
    "GH", "GHA", "288",
    "GI", "GIB", "292",
    "GD", "GRD", "308",
    "GR", "GRC", "300",
    "GL", "GRL", "304",
    "GB", "GBR", "826",
    "GP", "GLP", "312",
    "GU", "GUM", "316",
    "GT", "GTM", "320",
    "GG", "GGY", "831",
    "GN", "GIN", "324",
    "GW", "GNB", "624",
    "GY", "GUY", "328",
    "HT", "HTI", "332",
    "HN", "HND", "340",
    "HK", "HKG", "344",
    "IN", "IND", "356",
    "ID", "IDN", "360",
    "IM", "IMN", "833",
    "IQ", "IRQ", "368",
    "IR", "IRN", "364",
    "IE", "IRL", "372",
    "IS", "ISL", "352",
    "IL", "ISR", "376",
    "IT", "ITA", "380",
    "JM", "JAM", "388",
    "JP", "JPN", "392",
    "YE", "YEM", "887",
    "JE", "JEY", "832",
    "JO", "JOR", "400",
    "KY", "CYM", "136",
    "KH", "KHM", "116",
    "CM", "CMR", "120",
    "CA", "CAN", "124",
    "KZ", "KAZ", "398",
    "QA", "QAT", "634",
    "KE", "KEN", "404",
    "KG", "KGZ", "417",
    "KI", "KIR", "296",
    "CC", "CCK", "166",
    "CO", "COL", "170",
    "KM", "COM", "174",
    "CG", "COG", "178",
    "CD", "COD", "180",
    "KP", "PRK", "408",
    "KR", "KOR", "410",
    "HR", "HRV", "191",
    "CU", "CUB", "192",
    "KW", "KWT", "414",
    "LA", "LAO", "418",
    "LS", "LSO", "426",
    "LV", "LVA", "428",
    "LB", "LBN", "422",
    "LR", "LBR", "430",
    "LY", "LBY", "434",
    "LI", "LIE", "438",
    "LT", "LTU", "440",
    "LU", "LUX", "442",
    "MO", "MAC", "446",
    "MG", "MDG", "450",
    "MW", "MWI", "454",
    "MY", "MYS", "458",
    "MV", "MDV", "462",
    "ML", "MLI", "466",
    "MT", "MLT", "470",
    "MA", "MAR", "504",
    "MH", "MHL", "584",
    "MQ", "MTQ", "474",
    "MR", "MRT", "478",
    "MU", "MUS", "480",
    "YT", "MYT", "175",
    "MX", "MEX", "484",
    "FM", "FSM", "583",
    "MD", "MDA", "498",
    "MC", "MCO", "492",
    "MN", "MNG", "496",
    "ME", "MNE", "499",
    "MS", "MSR", "500",
    "MZ", "MOZ", "508",
    "MM", "MMR", "104",
    "NA", "NAM", "516",
    "NR", "NRU", "520",
    "NP", "NPL", "524",
    "NC", "NCL", "540",
    "NZ", "NZL", "554",
    "NI", "NIC", "558",
    "NL", "NLD", "528",
    "NE", "NER", "562",
    "NG", "NGA", "566",
    "NU", "NIU", "570",
    "MP", "MNP", "580",
    "MK", "MKD", "807",
    "NF", "NFK", "574",
    "NO", "NOR", "578",
    "OM", "OMN", "512",
    "AT", "AUT", "040",
    "PK", "PAK", "586",
    "PW", "PLW", "585",
    "PA", "PAN", "591",
    "PG", "PNG", "598",
    "PY", "PRY", "600",
    "PE", "PER", "604",
    "PH", "PHL", "608",
    "PN", "PCN", "612",
    "PL", "POL", "616",
    "PT", "PRT", "620",
    "PR", "PRI", "630",
    "RE", "REU", "638",
    "RW", "RWA", "646",
    "RO", "ROU", "642",
    "RU", "RUS", "643",
    "MF", "MAF", "663",
    "SB", "SLB", "090",
    "ZM", "ZMB", "894",
    "WS", "WSM", "882",
    "SM", "SMR", "674",
    "ST", "STP", "678",
    "SA", "SAU", "682",
    "SE", "SWE", "752",
    "CH", "CHE", "756",
    "SN", "SEN", "686",
    "RS", "SRB", "688",
    "SC", "SYC", "690",
    "SL", "SLE", "694",
    "ZW", "ZWE", "716",
    "SG", "SGP", "702",
    "SX", "SXM", "534",
    "SK", "SVK", "703",
    "SI", "SVN", "705",
    "SO", "SOM", "706",
    "ES", "ESP", "724",
    "SJ", "SJM", "744",
    "LK", "LKA", "144",
    "BL", "BLM", "652",
    "KN", "KNA", "659",
    "LC", "LCA", "662",
    "PM", "SPM", "666",
    "VC", "VCT", "670",
    "ZA", "ZAF", "710",
    "SD", "SDN", "729",
    "GS", "SGS", "239",
    "SS", "SSD", "728",
    "SR", "SUR", "740",
    "SY", "SYR", "760",
    "TJ", "TJK", "762",
    "TW", "TWN", "158",
    "TZ", "TZA", "834",
    "TH", "THA", "764",
    "TL", "TLS", "626",
    "TG", "TGO", "768",
    "TK", "TKL", "772",
    "TO", "TON", "776",
    "TT", "TTO", "780",
    "TD", "TCD", "148",
    "CZ", "CZE", "203",
    "TN", "TUN", "788",
    "TR", "TUR", "792",
    "TM", "TKM", "795",
    "TC", "TCA", "796",
    "TV", "TUV", "798",
    "UG", "UGA", "800",
    "UA", "UKR", "804",
    "HU", "HUN", "348",
    "UY", "URY", "858",
    "UZ", "UZB", "860",
    "VU", "VUT", "548",
    "VA", "VAT", "336",
    "VE", "VEN", "862",
    "AE", "ARE", "784",
    "US", "USA", "840",
    "VN", "VNM", "704",
    "WF", "WLF", "876",
    "CX", "CXR", "162",
    "PS", "PSE", "275",
    "EH", "ESH", "732",
    "CF", "CAF", "140",
    "CY", "CYP", "196"];

const gaiaXSelfDescription = {

    validate: async (selfdescription) => {
        try {
            const shapeInfo = shapes.filter(x => x.type === selfdescription["@type"])[0];
            const shape = await loadShapes(shapeInfo);

            if (shape) {

                //schema validation
                const data = await loadSelfDescription(selfdescription);
                const validator = new SHACLValidator(shape, { factory });

                const report = await validator.validate(data);

                let ret = [];

                for (const result of report.results) {
                    let msgs = [];
                    for (let index = 0; index < result.message.length; index++) {
                        msgs.push(result.message[index].value);
                    }

                    ret.push({
                        path: result.path.value,
                        messages: msgs
                    });
                }

                //content validation
                const contentValidation = shapeInfo.validate ? await shapeInfo.validate(selfdescription) : "content validation not available";

                return {
                    schema: {
                        isConform: report.conforms,
                        errors: ret
                    },
                    content: contentValidation
                };
            }
            else {
                return "Unprocessable Entity";
            }
        } catch (error) {
            return error;
        }
    }
}

async function loadShapes(shapeInfo) {
    let shapes;
    for (let index = 0; index < shapeInfo.shapes.length; index++) {
        if (shapes == null)
            shapes = await loadShape(shapeInfo.shapes[index]);
        else
            shapes.addAll(await loadShape(shapeInfo.shapes[index]));
    }

    return shapes;
}

async function loadShape(shapeLink) {
    let shape = "";
    if (shapeLink?.url) {
        let options = {};
        if (shapeLink.url.startsWith("https://")) {
            options.agent = new https.Agent({
                rejectUnauthorized: false
            });
        }

        const response = await fetch(shapeLink.url, options);
        shape = await response.text();
    }
    else if (shapeLink?.file) shape = fs.readFileSync(shapeLink.file, 'utf8');

    if (shape.length > 0) {

        var stream = new Readable();
        var shapeFormat = checkShapeFormat(shape);

        if (shapeFormat === "json") stream.push(json2ttl(JSON.parse(shape)));
        else if (shapeFormat === "ttl") stream.push(shape);
        else return null;

        stream.push(null);

        const parser = new ParserN3({ factory });
        return factory.dataset().import(parser.import(stream));
    }
    else return null;
}

async function loadSelfDescription(selfdescription) {
    var stream = new Readable();
    stream.push(JSON.stringify(selfdescription));
    stream.push(null);

    const parser = new ParserJsonld({ factory });
    return factory.dataset().import(parser.import(stream));
}

function checkShapeFormat(shape) {
    try {
        JSON.parse(shape);
    } catch (error) {
        return "ttl";
    }

    return "json";
}

function json2ttl(json_shape) {
    let ttl = "";

    const prefixList = json_shape.prefixList;
    for (let i = 0; i < prefixList.length; i++) {
        ttl += "@prefix " + prefixList[i].alias + ":\t<" + prefixList[i].url + "> .\n";
    }

    ttl += "\n";

    const shapes = json_shape.shapes;
    for (let i = 0; i < shapes.length; i++) {
        ttl += shapes[i].targetClassPrefix + ":" + shapes[i].targetClassName + "\n";
        ttl += "\t" + "a" + "\t" + "sh:NodeShape;" + "\n";
        ttl += "\t" + "sh:targetClass" + "\t" + shapes[i].targetClassPrefix + ":" + shapes[i].targetClassName + ";\n";

        const constraints = shapes[i].constraints;
        for (let a = 0; a < constraints.length; a++) {
            ttl += "\t" + "sh:property" + "\t" + "[ sh:path " + constraints[a].path.prefix + ":" + constraints[a].path.value + " ;\n";
            if (constraints[a].order) ttl += "\t\t" + "sh:order " + constraints[a].order + " ;\n";
            if (constraints[a].name) ttl += "\t\t" + "sh:name \"" + constraints[a].name + "\" ;\n";
            if (constraints[a].description) ttl += "\t\t" + "sh:description \"" + constraints[a].description + "\" ;\n";
            if (constraints[a].minCount) ttl += "\t\t" + "sh:minCount " + constraints[a].minCount + " ;\n";
            if (constraints[a].maxCount) ttl += "\t\t" + "sh:maxCount " + constraints[a].maxCount + " ;\n";
            if (constraints[a].minLength) ttl += "\t\t" + "sh:minLength " + constraints[a].minLength + " ;\n";
            if (constraints[a].maxLength) ttl += "\t\t" + "sh:maxLength " + constraints[a].maxLength + " ;\n";
            if (constraints[a].pattern) ttl += "\t\t" + "sh:pattern \"" + constraints[a].pattern + "\" ;\n";
            if (constraints[a].flags) ttl += "\t\t" + "sh:flags \"" + constraints[a].flags + "\" ;\n";
            if (constraints[a].children) {
                var targetShape = json_shape.shapes.filter(x => x.schema === constraints[a].children)[0];
                if (targetShape) ttl += "\t\t" + "sh:targetNode " + targetShape.targetClassPrefix + ":" + targetShape.targetClassName + "\n";
            }
            if (constraints[a].datatype) {
                if (constraints[a].datatype.prefix === "nodeKind") ttl += "\t\t" + "sh:nodeKind sh:" + constraints[a].datatype.value + "\n";
                else ttl += "\t\t" + "sh:datatype " + constraints[a].datatype.prefix + ":" + constraints[a].datatype.value + "\n";
            }
            ttl += "];\n";
        }
        ttl += ".\n";
    }

    return ttl;
}

async function validateProperty(selfdescription, propertyName, validateFnc) {
    var ret = true;

    var properties = searchProperties(selfdescription, propertyName);
    for (let index = 0; index < properties.length; index++) {
        ret = await validateFnc(properties[index]["@value"]);
        if (!ret) break;
    }

    return {
        result: ret,
        msg: !ret ? propertyName + " invalid" : ""
    };
}

function searchProperties(obj, searchKey, results = []) {
    const r = results;
    Object.keys(obj).forEach(key => {
        const value = obj[key];
        if (key === searchKey) {
            r.push(value);
        } else if (typeof value === 'object') {
            searchProperties(value, searchKey, r);
        }
    });
    return r;
};

async function validation_gaxParticipantLegalPerson(selfdescription) {
    var leiCheck = await validateProperty(selfdescription, "gax-participant:leiCode", checkLeiCode);
    var isoCheck = await validateProperty(selfdescription, "gax-participant:country", checkISO3166Code);

    var msgs = [];
    if (leiCheck.msg.length > 0) msgs.push(leiCheck.msg);
    if (isoCheck.msg.length > 0) msgs.push(isoCheck.msg);

    return {
        isValid: (leiCheck.result & isoCheck.result) === 1,
        errors: msgs
    }
}

async function checkLeiCode(leiCode) {
    try {
        const response = await fetch("https://api.gleif.org/api/v1/lei-records/" + leiCode);
        const gleifResult = await response.json();
        return true;
    }
    catch (error) {
        return false;
    }

}
async function checkISO3166Code(isoCode) {
    return iso3166List.indexOf(isoCode) > -1 ? true : false;
}

export default gaiaXSelfDescription;
