# Gaia-X self-description validation module

## Name
gx-lib-sd-validation

## Description
This module is a work in progress.

## Authors and acknowledgment
Thanks to all authors / contributors of the following modules.
- [@rdfjs/parser-jsonld](https://github.com/rdfjs-base/parser-jsonld)
- [@rdfjs/parser-n3](https://github.com/rdfjs-base/parser-n3)
- [node-fetch](https://github.com/node-fetch/node-fetch)
- [rdf-ext](https://github.com/rdf-ext/rdf-ext)
- [rdf-validate-shacl](https://github.com/zazuko/rdf-validate-shacl)

## License
[Eclipse Public License - v 2.0](https://www.eclipse.org/legal/epl-2.0/) 