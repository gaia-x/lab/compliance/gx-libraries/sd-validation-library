 const { default: gaiaXSelfDescription } = await import("../src/index.js");

const fraunhoferTest = await gaiaXSelfDescription.validate({ 
    "@context": {
        "gax-participant": "http://w3id.org/gaia-x/participant#",
        "gax-service": "http://w3id.org/gaia-x/service#",
        "dct": "http://purl.org/dc/terms/",
        "sh": "http://www.w3.org/ns/shacl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#",
        "gax-validation": "http://w3id.org/gaia-x/validation#",
        "gax-node": "http://w3id.org/gaia-x/node#",
        "vcard": "http://www.w3.org/2006/vcard/ns#",
        "dcat": "http://www.w3.org/ns/dcat#",
        "gax-resource": "http://w3id.org/gaia-x/resource#",
        "gax-core": "http://w3id.org/gaia-x/core#"
    },
    "@id": "http://example.org/Provider-jkultthrxdl1l6vxjhm7qt",
    "@type": "gax-participant:Provider",
    "gax-participant:hasLegalForm": {
        "@value": "GmbH",
        "@type": "xsd:string"
    },
    "gax-participant:hasLegallyBindingName": {
        "@value": "AMCE",
        "@type": "xsd:string"
    },
    "gax-participant:hasLegallyBindingAddress": {
        "@type": "vcard:Address",
        "vcard:street-address": {
            "@value": "ACME Street 1",
            "@type": "xsd:string"
        },
        "vcard:postal-code": {
            "@value": "01069",
            "@type": "xsd:string"
        },
        "vcard:country-name": {
            "@value": "Germany",
            "@type": "xsd:string"
        },
        "vcard:locality": {
            "@value": "Dresden",
            "@type": "xsd:string"
        }
    },
    "gax-participant:hasJurisdiction": {
        "@value": "Germany",
        "@type": "xsd:string"
    },
    "gax-participant:hasVATnumber": {
        "@value": "123456789",
        "@type": "xsd:string"
    },
    "gax-participant:hasLegalRegistrationNumber": {
        "@value": "1234565789",
        "@type": "xsd:string"
    },
    "gax-participant:hasWebAddress": {
        "@value": "ww.acme.com",
        "@type": "xsd:anyURI"
    },
    "gax-participant:hasIndividualContactLegal": {
        "@type": "vcard:Agent",
        "vcard:given-name": {
            "@value": "Jane",
            "@type": "xsd:string"
        },
        "vcard:family-name": {
            "@value": "Dow",
            "@type": "xsd:string"
        },
        "vcard:nickname": {
            "@value": "Jane",
            "@type": "xsd:string"
        },
        "vcard:hasEmail": {
            "@value": "jane.dow@acme.com",
            "@type": "xsd:anyURI"
        }
    },
    "gax-participant:hasIndividualContactTechnical": {
        "@type": "vcard:Agent",
        "vcard:given-name": {
            "@value": "Jon",
            "@type": "xsd:string"
        },
        "vcard:family-name": {
            "@value": "Dow",
            "@type": "xsd:string"
        },
        "vcard:nickname": {
            "@value": "Jon",
            "@type": "xsd:string"
        },
        "vcard:hasEmail": {
            "@value": "jon.dow@acme.com",
            "@type": "xsd:anyURI"
        }
    }
});

console.log("Test Fraunhofer Shape: " + JSON.stringify(fraunhoferTest));

 
const gxLabTest_Simple = await gaiaXSelfDescription.validate({
    "@context": {
        "gax-participant": "http://w3id.org/gaia-x/participant#",
        "sh": "http://www.w3.org/ns/shacl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@id": "http://example.org/participant-sample-jkultthrxdl1l6vxjhm7qt",
    "@type": "gax-participant:legal-person",
    "gax-participant:registrationNumber": {
        "@value": "DEK1101R.HRB170364",
        "@type": "xsd:string"
    },
    "gax-participant:headquarterAddress": {
        "@type": "gax-participant:country",
        "gax-participant:country": {
            "@value": "DE",
            "@type": "xsd:string"
        }
    },
    "gax-participant:legalAddress": {
        "@type": "gax-participant:country",
        "gax-participant:country": {
            "@value": "DE",
            "@type": "xsd:string"
        }
    },
    "gax-participant:leiCode": {
        "@value": "391200FJBNU0YW987L26",
        "@type": "xsd:string"
    }
});

console.log("Test GX-Lab Shape Simple: " + JSON.stringify(gxLabTest_Simple));

const gxLabTest = await gaiaXSelfDescription.validate({
    "@context": {
        "gax-participant": "http://w3id.org/gaia-x/participant#",
        "sh": "http://www.w3.org/ns/shacl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@id": "http://example.org/participant-sample-jkultthrxdl1l6vxjhm7qt",
    "@type": "gax-participant:legal-person",
    "gax-participant:registrationNumber": {
        "@value": "our company",
        "@type": "xsd:string"
    },
    "gax-participant:headquarterAddress": {
        "@type": "gax-participant:country",
        "gax-participant:country": {
            "@value": "DE",
            "@type": "xsd:string"
        }
    },
    "gax-participant:legalAddress": {
        "@type": "gax-participant:country",
        "gax-participant:country": {
            "@value": "DE",
            "@type": "xsd:string"
        }
    },
    "gax-participant:leiCode": {
        "@value": "391200FJBNU0YW987L26",
        "@type": "xsd:string"
    },
    "gax-participant:parentOrganisation": [
        {
            "@type": "gax-participant:legal-person",
            "gax-participant:registrationNumber": {
                "@value": "the holding",
                "@type": "xsd:string"
            },
            "gax-participant:headquarterAddress": {
                "@type": "gax-participant:country",
                "gax-participant:country": {
                    "@value": "UK",
                    "@type": "xsd:string"
                }
            },
            "gax-participant:legalAddress": {
                "@type": "gax-participant:country",
                "gax-participant:country": {
                    "@value": "FR",
                    "@type": "xsd:string"
                }
            },
            "gax-participant:leiCode": {
                "@value": "391200FJBNU0YW987L27",
                "@type": "xsd:string"
            }
        }
    ],
    "gax-participant:subOrganisation": [
        {
            "@type": "gax-participant:legal-person",
            "gax-participant:registrationNumber": {
                "@value": "subsidiary 1",
                "@type": "xsd:string"
            },
            "gax-participant:headquarterAddress": {
                "@type": "gax-participant:country",
                "gax-participant:country": {
                    "@value": "UK",
                    "@type": "xsd:string"
                }
            },
            "gax-participant:legalAddress": {
                "@type": "gax-participant:country",
                "gax-participant:country": {
                    "@value": "FR",
                    "@type": "xsd:string"
                }
            },
            "gax-participant:leiCode": {
                "@value": "391200FJBNU0YW987L25",
                "@type": "xsd:string"
            }
        },
        {
            "@type": "gax-participant:legal-person",
            "gax-participant:registrationNumber": {
                "@value": "subsidiary 2",
                "@type": "xsd:string"
            },
            "gax-participant:headquarterAddress": {
                "@type": "gax-participant:country",
                "gax-participant:country": {
                    "@value": "UK",
                    "@type": "xsd:string"
                }
            },
            "gax-participant:legalAddress": {
                "@type": "gax-participant:country",
                "gax-participant:country": {
                    "@value": "FR",
                    "@type": "xsd:string"
                }
            },
            "gax-participant:leiCode": {
                "@value": "391200FJBNU0YW987L24",
                "@type": "xsd:string"
            }
        }
    ]
});

console.log("Test GX-Lab Shape: " + JSON.stringify(gxLabTest));

const ctxTest = await gaiaXSelfDescription.validate({
    "@context": {
        "ctx": "https://catena-x.net/selfdescription#",
		"gax-participant": "http://w3id.org/gaia-x/participant#",
        "sh": "http://www.w3.org/ns/shacl#",
        "xsd": "http://www.w3.org/2001/XMLSchema#"
    },
    "@id": "http://example.org/sd-sample-abultthrxdl1l6vxjhm7qt",
    "@type": "ctx:sd-document",
	"ctx:legal-person": {
      "@type": "gax-participant:legal-person",
      "gax-participant:registrationNumber": {
        "@value": "DEK1101R.HRB170364",
        "@type": "xsd:string"
      },
      "gax-participant:headquarterAddress": {
        "@type": "gax-participant:country",
        "gax-participant:country": {
          "@value": "DE",
          "@type": "xsd:string"
        }
      },
      "gax-participant:legalAddress": {
        "@type": "gax-participant:country",
        "gax-participant:country": {
          "@value": "D",
          "@type": "xsd:string"
        }
      },
      "gax-participant:leiCode": {
        "@value": "391200FJBNU0YW987L26",
        "@type": "xsd:string"
      }
    },
    "ctx:service_provider": {
        "@value": "https://www.delta-dao.com",
        "@type": "xsd:string"
    },
	"ctx:sd_type": {
        "@value": "???",
        "@type": "xsd:string"
    },
    "ctx:bpn": {
        "@value": "???",
        "@type": "xsd:string"
    }
});

console.log("Test CTX Shape: " + JSON.stringify(ctxTest));
